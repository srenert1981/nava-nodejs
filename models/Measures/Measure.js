class Measure {
    #fields = [];

    addField(dataField) {
        this.#fields.push(dataField);
    }

    // build json string based on all data fields in a measure
    getJSON() {
        let json = {};
        this.#fields.forEach(field => {
            json[field.name] = field.value;
        });
        return JSON.stringify(json);
    }
}
module.exports = Measure;
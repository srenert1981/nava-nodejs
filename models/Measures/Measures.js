const { dirname } = require('path');
const path = require('path');
const Schema = require('../Schema/Schema')
const fs = require('fs');
const os = require('os');
const DataField = require('../DataField/DataField');
const Measure = require('./Measure');

class Measures {
    #schema = new Schema();
    #measures = [];
    constructor(schema) {
        this.#schema = schema;
    }
    getMeasures() {
        return this.#measures;
    }
    load() {
        let added = 0;
        const root = dirname(require.main.filename);

        // get data file name based on name of schema
        const dataPath = path.normalize(root + '/data/' + this.#schema.getFileName()).replaceAll(".csv",".txt");
        console.log(`dataPath: ${dataPath}`);

        if (fs.existsSync(dataPath)) {
            const contents = fs.readFileSync(dataPath, { encoding: 'utf8', flag: 'r' });
            const lines = contents.split(os.EOL);
            lines.forEach(line => {
                let startIdx = 0;
                // instantiate new Measure
                let measure = new Measure();

                // parse line and find data fields
                this.#schema.getDataFields().forEach(schemaField => {
                    let item = line.substring(startIdx, startIdx+schemaField.getWidth());
                    // create new data field based on type
                    let dataField = DataField.create(schemaField.getSqlType(), schemaField.getFieldName(), item);
                    measure.addField(dataField);

                    // increment position in the line string
                    startIdx+= schemaField.getWidth();
                });
                this.#measures.push(measure);
                added++;
            });
        }
        return added > 0;
    }
}
module.exports = Measures;
class SchemaField {
    #fieldName = '';
    #width = 0;
    #sqlType = 'TEXT';

    constructor(fieldName, width, sqlType) {
        if (isNaN(width)) throw (`Width ${width} is not a valid number!`);
        if (!sqlType.match(/INTEGER|BOOLEAN|TEXT/)) throw (`SQLType ${sqlType} is not allowed`);

        this.#fieldName = fieldName;
        this.#width = parseInt(width);
        this.#sqlType = sqlType;
    }
    getFieldName() {
        return this.#fieldName;
    }
    getWidth() {
        return this.#width;
    }
    getSqlType() {
        return this.#sqlType;
    }
}

module.exports = SchemaField;
const fs = require('fs');
const os = require('os');
const SchemaField = require('./SchemaField');

class Schema {
    #rootPath = '';
    #fileName = '';
    #dataFields = [];
    constructor(rootPath, fileName) {
        this.#rootPath = rootPath;
        this.#fileName = fileName;
    }
    getFileName() {
        return this.#fileName;
    }
    getDataFields() {
        return this.#dataFields;
    }
    parse() {
        const filePath = this.#rootPath + "/" + this.#fileName;
        console.log(`Reading ${this.#fileName} from ${filePath}`);
        const contents = fs.readFileSync(filePath, { encoding: 'utf8', flag: 'r'});
        if (!contents) throw `${this.#fileName} is missing schema`;
        const lines = contents.split(os.EOL);
        lines.forEach(line => {
            let cols = line.split(",");
            let schemaField = new SchemaField(cols[0], cols[1], cols[2]);
            this.#dataFields.push(schemaField);
        })
    }
    static getAllSchemas(root) {
        let schemas = [];
        console.log(`Getting all schemas from ${root}`);
        const files = fs.readdirSync(root);
        files.forEach(file => {
            let schema = new Schema(root, file);
            schema.parse();
            schemas.push(schema);
        })
        return schemas;
    }
}
module.exports = Schema;
const DataField = require('./DataField');

class IntegerDataField extends DataField {
    constructor(name, value) {
        super();

        this._name = name;
        this._value = value;
    }
    // always return int
    get value() {
        return parseInt(this._value);
    }
}
module.exports = IntegerDataField;
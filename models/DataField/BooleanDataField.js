const DataField = require('./DataField');

class BooleanDataField extends DataField {
    constructor(name, value) {
        super();

        this._name = name;
        this._value = value;
    }
    // always return bool
    get value() {
        return (this._value ? true : false);
    }
}
module.exports = BooleanDataField;
const DataField = require('./DataField');

class TextDataField extends DataField {
    constructor(name, value) {
        super();

        this._name = name;
        this._value = value;
    }
}
module.exports = TextDataField;
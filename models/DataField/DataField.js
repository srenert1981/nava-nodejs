class DataField {
    _name;
    _value;

    constructor() {
    }

    // return specific data field based on type
    static create(type, name, value) {
        switch (type) {
            case 'INTEGER':
                const IntegerDataField = require('./TextDataField');
                return new IntegerDataField(name, value);
            case 'TEXT':
                const TextDataField = require('./TextDataField');
                return new TextDataField(name, value);
            case 'BOOLEAN':
                const BooleanDataField = require('./BooleanDataField');
                return new BooleanDataField(name, value);
        }
    }
    get name() {
        return this._name;
    }
    get value() {
        return this._value;
    }
}
module.exports = DataField;
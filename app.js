const Schema = require('./models/Schema/Schema');
const Measures = require('./models/Measures/Measures');
const { dirname } = require('path');
const path = require('path');
const root = dirname(require.main.filename);
const schemaPath = path.normalize(root + '/schemas') //'/users/test.txt'
const axios = require('axios');

function process() {

    console.log('root: ' + root);
    console.log('schemaPath: ' + schemaPath);

    // find all schemas in a directory
    const schemas = Schema.getAllSchemas(schemaPath);

    schemas.forEach(schema => {
        console.log(schema.getFileName());
        let measures = new Measures(schema);

        // find all measures in data directory for current schema
        if (measures.load()) {

            // loop through each measure and post to endpoint
            measures.getMeasures().forEach(measure => {
                console.log(measure.getJSON());
                axios.post('https://2swdepm0wa.execute-api.us-east-1.amazonaws.com/prod/NavaInterview/measures', measure.getJSON())
                    .then(res => {
                        if (res.status === 201) console.log('Success!');
                        else console.error('Failed to send measure!');
                    });
            })
        }
    });
}

process();

# README #

Nava Software Engineer Takehome Exercise.

### What is the purpose of this project? ###

* Iterate through potential measures and post to API
* Version 1.0

### How do I get set up? ###

* ```git clone https://srenert1981@bitbucket.org/srenert1981/nava-nodejs.git```
* Make sure that Node.JS and npm are installed
* Install axios using the following command ```npm install axios```
* Go to root of project in terminal
* Run ```node app.js```

### Who do I talk to? ###

* Simon Renert (srenert@gmail.com)